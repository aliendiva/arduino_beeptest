# Arduino Beeptest
Audio Unit for The multi-stage fitness test, aka PACER test or 20 m Shuttle Run used by Australian Special Forces, Military and Australia Federal Police as a Preliminary Fitness Test Standard required prior to direct recruitment scheme.

This repository holds files for construction of the Audio Unit prototype and software to control functionality.

This work is licensed under the Attribution-NonCommercial-NoDerivs 3.0 Australia (CC BY-NC-ND 3.0 AU) License.

## Button 1 (play/pause)
Plays the audio file from start if not playing. Pause/resume play if already started.

## Button 2
Stops the audio playing unit sleeps. Long press (>2 seconds) resets the player, there is an audible click.

## Hardware Required
- 1 x DFPlayer Mini audio module
- 1 x Micro controller unit (Arduino Mini Pro -or- Wemos D1 mini Esp8266)
- 1 x 10k Potentiometer
- 2 x Momentary push buttons
- 1 x 8ohm horn speaker

![Schematic](https://gitlab.com/aliendiva/arduino_beeptest/raw/master/schematic.png)
