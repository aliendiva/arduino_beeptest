#include <Bounce2.h>
#include <DFPlayerMini_Fast.h>
#include <SoftwareSerial.h>

DFPlayerMini_Fast df;
SoftwareSerial ss(4, 5); // Set up Software serial IO
Bounce btn1 = Bounce();  // Enable debouncing on button 1
Bounce btn2 = Bounce();  // Enable debouncing on button 1

bool paused = false;  // Define paused state variable
int volume = 15;      // Define paused level variable
unsigned long btn2ts; // Timestamp for longpress on btn2

void setup() {
  ss.begin(9600); // Software serial RX/TX on DFPlayer
  df.begin(ss);   // DFPlayerMini_Fast connect to Software Serial

  pinMode(10, INPUT_PULLUP); // Button 1
  pinMode(11, INPUT_PULLUP); // Button 2
  pinMode(12, INPUT);        // Busy pin on DFPlayer
  pinMode(A1, INPUT);        // Potentiometer to set volume
  btn1.attach(10);           // Attach debouncer to btn1
  btn2.attach(11);           // Attach debouncer to btn2

  volume = (analogRead(A1) / 10.24); // Convert volume 0..1024 to 0..100
}

void loop() {

  btn1.update(); // Update debouncer for btn1
  btn2.update(); // Update debouncer for btn2

  // Volume Pot: check for change
  int pot = (analogRead(A1) / 10.24);
  if (pot < volume - 1 || pot > volume + 1) { // Fix toggle between 2 values
    volume = pot;                    // Volime pot changed. Update volume.
    df.volume((int)(volume / 3.33)); // Set player volume 0..30
  }

  // Button 1: push play/pause
  if (btn1.fell()) {
    df.volume((int)(volume / 3.33)); // Set volume from setup()

    if (digitalRead(12) == 1) { // Read busy pin on DFPlayer
      if (paused) {
        df.resume();      // Unpause DFPlayer
        paused = !paused; // Update Paused state
      } else {
        df.play(1); // Play mp3 #1
      }
    }

    else {
      df.pause();       // Pause DFPlayer
      paused = !paused; // Update Paused state
    }
  }

  // Button 2: stop
  if (btn2.fell()) {
    df.play(0);        // Play nothing
    btn2ts = millis(); // Set timestamp for long press
  }

  // Button 2: reset (long press 2 seconds)
  if (btn2.read() == LOW && (millis() - btn2ts) > 2000) {
    df.reset();        // Reset DFPlayer
    btn2ts = millis(); // Reset timestamp for long press
  }
}
